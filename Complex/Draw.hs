module Complex.Draw
(
drawSet,
red, green, blue, yellow, magenta, cyan,
vertex3f
)
where
import Graphics.UI.GLUT
import Data.Complex

data Box = Box Float Float Float Float deriving (Show, Eq)

color3f :: GLfloat -> GLfloat -> GLfloat -> IO ()
color3f r g b = color $ Color3 r g (b :: GLfloat)

vertex3f :: GLfloat -> GLfloat -> GLfloat -> IO ()
vertex3f x y z = vertex $ Vertex3 x y (z :: GLfloat)

red = color3f 1 0 0
green = color3f 0 1 0
blue = color3f 0 0 1
yellow = color3f 1 1 0
magenta = color3f 1 0 1
cyan = color3f 0 1 1

getColor :: Int -> IO ()
getColor 0 = red
getColor 1 = green
getColor 2 = blue
getColor 3 = yellow
getColor k = getColor (k `mod` 4)


getMinMax :: [(Complex Float, t)] -> Box
getMinMax [] = Box 0 1 0 1
getMinMax ((z@(x :+ y), _):t) =
  Box (min x xmin) (max x xmax) (min y ymin) (max y ymax)
  where Box xmin xmax ymin ymax = getMinMax t


plot :: Box -> (Complex Float, Int) -> DisplayCallback
plot (Box xmin xmax ymin ymax) (node, k) = do
  let xnorm = xmax - xmin
      ynorm = ymax - ymin
      x = 2 * (realPart node - xmin) / xnorm - 1
      y = 2 * (imagPart node - ymin) / ynorm - 1
  do
    getColor k
    vertex3f x y 0

drawSet :: [(Complex Float, Int)] -> DisplayCallback
drawSet complexSet = do
  let 
      Box xmin xmax ymin ymax = getMinMax complexSet
  print (Box xmin xmax ymin ymax)
  clear [ColorBuffer]
  renderPrimitive Points $
    mapM_ (plot (Box xmin xmax ymin ymax)) complexSet
  flush
