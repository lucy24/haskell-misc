module Complex
where

  data Complex = Complex 
	  { r :: Float
	  , i :: Float
	  } deriving (Eq, Show, Read)

  magnitude :: Complex -> Float
  magnitude (Complex { r = x, i = y }) =  scaleFloat k
                       (sqrt (sqr (scaleFloat mk x) + sqr (scaleFloat mk y)))
                      where k  = max (exponent x) (exponent y)
                            mk = - k
                            sqr z = z * z

  instance  Num (Complex)  where
      (Complex { r = x1, i = y1 }) + (Complex { r = x2, i = y2 })   =  Complex { r = x1 + x2, i = y1 + y2 }
      (Complex { r = x1, i = y1 }) - (Complex { r = x2, i = y2 })   =  Complex { r = x1 - x2, i = y1 - y2 }
      (Complex { r = x1, i = y1 }) * (Complex { r = x2, i = y2 })   =  Complex { r = x1 * x2 - y1 * y2, i = x1 * y2 + y1 * x2 }
      negate (Complex { r = x, i = y })   =  Complex { r = negate x, i = negate y }
      abs z                               =  Complex { r = magnitude z, i = 0 }
      signum (Complex { r = 0, i = 0 })   =  0
      signum z@(Complex { r = x, i = y }) =  Complex { r = x/r, i = y/r } where r = magnitude z
      fromInteger n                       =  Complex { r = fromInteger n, i = 0 }

  zero = Complex { r = 0, i = 0 }
  un = Complex { r = 1, i = 0 }
  ic = Complex { r = 0, i = 1 }
