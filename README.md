# First steps with Haskell

## Fractals with complex numbers

This is something I had done during my first programming years (then in Caml). Depending on the initial value, we can draw very beautiful fractals.

TODO

- adapt bounding box "on the fly" and reduce memory usage.

## Julia sets

For the Julia sets, I used the algorithm described [here](http://eldar.mathstat.uoguelph.ca/dashlock/ftax/Julia.html).