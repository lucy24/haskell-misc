import Graphics.UI.GLUT
import Data.Complex

-- Julia set functions
-- algorithm from http://eldar.mathstat.uoguelph.ca/dashlock/ftax/Julia.html
nextjuliaNb mu z = z * z + mu

isInJuliaSet z = magnitude z < 2

computeJuliaN k mu n z
  | k >= n = z
  | magnitude z > 2 = z
  | otherwise = computeJuliaN (k+1) mu n (z * z + mu)

computeJulia = computeJuliaN 0

-- color plot helpers
color3f :: GLfloat -> GLfloat -> GLfloat -> IO ()
color3f r g b = color $ Color3 r g (b :: GLfloat)


red = color3f 1 0 0
green = color3f 0 1 0
blue = color3f 0 0 1
yellow = color3f 1 1 0
magenta = color3f 1 0 1
cyan = color3f 0 1 1


juliaColor z 
  | isInJuliaSet z = red
  | otherwise = yellow


-- plot helper
vertex3f :: GLfloat -> GLfloat -> GLfloat -> IO ()
vertex3f x y z = vertex $ Vertex3 x y (z :: GLfloat)


plotJuliaPoint mu n z = do
  let 
    z' = computeJulia mu n (1.3*z)
    (x :+ y) = z
  renderPrimitive Points $ do
    juliaColor z'
    vertex3f x y 0 

-- Plot the Julia set (only 2 colors)
plotJulia mu n = do
  let 
    xx = take 550 [0, 0.002..]
    windowPoints = [ (2*x-1) :+ (2*y-1) | x <- xx, y <- xx]
  clear [ColorBuffer]
  mapM_ (plotJuliaPoint mu n) windowPoints


main :: IO ()
main = do
  (_progName, _args) <- getArgsAndInitialize
  _window <- createWindow "A complex Fractal"
  displayCallback $= plotJulia (0.285 :+ 0.013) 100
  mainLoop


