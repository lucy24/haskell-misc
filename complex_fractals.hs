import Graphics.UI.GLUT
import Data.Complex
import qualified Complex.Draw as CD


data Box = Box Float Float Float Float deriving (Show, Eq)

data NodeVal = NodeVal (Complex Float) Int deriving (Show, Eq)
data Tree a = EmptyTree | Node a (Tree a) (Tree a) deriving (Eq, Show)


zero = 0 :+ 0
un = 1 :+ 0
ic = 0 :+ 1

-- Compute tree
-- A binary tree with complex parameter b
-- starting with value z=1 and for each node with value z, 
-- left_child takes value b*z, and right_child b*z+1
-- stop for given value n
buildComplexTree :: Complex Float -> Int -> Tree NodeVal
buildComplexTreeAux (NodeVal node k) b n
  | k==n = Node (NodeVal node k) EmptyTree EmptyTree
  | k<n = Node (NodeVal node k) 
               (buildComplexTreeAux (NodeVal (b*node) (k+1)) b n)
               (buildComplexTreeAux (NodeVal (b*node + 1) (k+1)) b n)

buildComplexTree = buildComplexTreeAux (NodeVal un 0)

buildComplexSet :: Tree NodeVal -> [(Complex Float, Int)]
buildComplexSet EmptyTree = []
buildComplexSet (Node (NodeVal node k) t1 t2) = (node, k):buildComplexSet t1 ++ buildComplexSet t2

-- Julia set functions
-- algorithm from http://eldar.mathstat.uoguelph.ca/dashlock/ftax/Julia.html
nextjuliaNb mu z = z * z + mu

isInJuliaSet z = magnitude z < 2

juliaColor1 z 
  | isInJuliaSet z = CD.red
  | otherwise = CD.yellow


computeJuliaN k mu n z
  | k >= n = z
  | realPart (abs z) > 2 = z
  | otherwise = computeJuliaN (k+1) mu n (z * z + mu)

computeJulia = computeJuliaN 0

plotJuliaPoint mu n z = do
  let 
    z' = computeJulia mu n (2*z)
    (x :+ y) = z
  renderPrimitive Points $ do
    juliaColor1 z'
    CD.vertex3f x y 0

xx = take 550 [0, 0.002..]
windowPoints = [ (2*x-1) :+ (2*y-1) | x <- xx, y <- xx]

-- Plot the Julia set (only 2 colors)
plotJulia mu n = do
  clear [ColorBuffer]
  mapM_ (plotJuliaPoint mu n) windowPoints

-- Plot the <complex fractal from binary tree>
plotComplex :: Complex Float-> Int -> DisplayCallback
plotComplex b n = do
  let 
      complexSet = buildComplexSet (buildComplexTree b n)
  CD.drawSet complexSet

main :: IO ()
main = do
  (_progName, _args) <- getArgsAndInitialize
  _window <- createWindow "A complex Fractal"
  displayCallback $= plotJulia (0.285 :+ 0.013) 100
--  displayCallback $= plotComplex (0.99 :+ 0.99) 16
  mainLoop


